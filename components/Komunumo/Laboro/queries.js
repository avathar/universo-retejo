// Запрос для получения данных конкретного сообщества
export const kombatantoj_query = `
  query ($uzantoId: Float) {
    kombatantoj (uzantoId:$uzantoId, orderBy: ["uzanto"]) {
      edges {
        node {
          uuid
          hid
          tipo {
            kodo
            nomo {
              enhavo
            }
          }
          nomo {
            enhavo
          }
          speco {
            kodo
          }
          familinomo {
            enhavo
          }
          unuaNomo {
            enhavo
          }
          uzanto {
            objId
            familinomo{
              enhavo
            }
            unuaNomo{
              enhavo
            }
          }
        }
        }
  
    }
  }`;

  export const kombatantoj_list_query = `
  query {
    kombatantoj (orderBy: ["uzanto"]) {
      edges {
        node {
          uuid
          hid
          tipo {
            kodo
            nomo {
              enhavo
            }
          }
          nomo {
            enhavo
          }
          speco {
            kodo
          }
          familinomo {
            enhavo
          }
          unuaNomo {
            enhavo
          }
          uzanto {
            objId
            familinomo{
              enhavo
            }
            unuaNomo{
              enhavo
            }
          }
        }
        }
  
    }
  }`;

export const komunumo_query = `
query($id: Float!) {
  komunumoj(objId: $id) {
    edges {
      node {
        id
        objId
        uuid
        tipo {
          kodo
          nomo {
            enhavo
          }
        }
        nomo {
          enhavo
        }
        priskribo {
          enhavo
        }
        statistiko {
          postulita
          tuta
          mia
          membraTipo
        }
        avataro {
          bildoE {
            url
          }
          bildoF {
            url
          }
        }
        kovrilo {
          bildoE {
            url
          }
        }
        informo {
          enhavo
        }
        informoBildo {
          bildo
          bildoMaks
        }
        kontaktuloj {
          edges {
            node {
              objId
              unuaNomo {
                enhavo
              }
              familinomo {
                enhavo
              }
              avataro {
                bildoF {
                  url
                }
              }
              kontaktaInformo
            }
          }
        }
        rajtoj
      }
    }
  }
}`;